<?php

namespace Tests\Feature;

use Tests\TestCase;

class CheckDataControllerTest extends TestCase
{
    public function testCheckData()
    {
        $data = [
            "foo" => 123,
            "bar" => "asd",
            "phone_number" => "8 (707) 288-56-23",
        ];

         $this->json('POST', 'api/check',$data , ['Accept' => 'application/json'])
         ->assertStatus(200)
         ->assertSuccessful();
    }

    public function testWrongData()
    {
        $this->json('POST', 'api/check', [
            'foo' => 'abc',
            'bar' => '123asd',
            'phone_number' => '12345',
            ], ['Accept' => 'application/json'])->assertStatus(422);
    }
}
