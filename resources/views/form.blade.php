<head>
    <link rel="stylesheet" href="/app.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<form id="check">
    @csrf

    <label for="foo">Foo:</label>
    <input type="text" name="foo" id="foo" value="{{ old('foo') }}" required>

    <label for="bar">Bar:</label>
    <input type="text" name="bar" id="bar" value="{{ old('bar') }}" required>

    <label for="phone_number">Phone Number:</label>
    <input type="text" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" required>

    <button type="submit">Отправить</button>
</form>
</body>
<script>
    $(document).ready(function(){
        $("#check").submit(function(event) {
            event.preventDefault();
            var formData = {
                foo: $('#foo').val(),
                bar: $('#bar').val(),
                phone_number: $('#phone_number').val(),
                _token: '{{ csrf_token() }}'
            };
            console.log(formData);

            $.ajax({
                url: '{{ route("check.data") }}',
                method: 'POST',
                data: formData,
                dataType: 'json',
                success: function (response) {
                    alert("Success: " + JSON.stringify(response));
                    $('#check')[0].reset();
                },
                error: function (error) {
                    var errorResponse = JSON.parse(error.responseText);
                    var errorMessage = errorResponse.message;
                    alert("Error: " + errorMessage);
                }
            });
        });
    });
</script>
