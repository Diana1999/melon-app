<?php

namespace App\Http\Controllers;

use App\Http\Requests\checkDataRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Repositories\checkDataRepo;


class CheckDataController extends Controller
{
    public function __construct(checkDataRepo $repo)
    {
        $this->repo = $repo;
    }

    public function checkData(checkDataRequest $request)
    {
        $data = $request->validated();
        $checkBar = $this->checkBar($data['bar']);
        $checkNumber = $this->checkNumber($data['phone_number']);
        $newData =
            [
                'foo' =>$data['foo'],
                'bar' =>$checkBar,
                'phone_number' =>$checkNumber
            ];

        $this->repo->create($newData);

        if ($request->expectsJson()) {
            return response()->json(['message' => 'Data processed successfully'], 200);
        } else {
            return redirect()->route('home')->with('success', 'Data processed successfully');
        }
    }

    public function checkBar(string $bar)
    {
        if (!preg_match('/\d/', $bar)) {
            return $bar;
        } else {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'message' => ['field :bar must not contain numbers =>' . " " . $bar]
            ]);
        }
    }

    public function checkNumber(string $number)
    {
        $cleanNumber = preg_replace('/[^0-9]/', '', $number);

        if (preg_match('/^7\d{10}$/', $cleanNumber)) {
            return '7' . substr($cleanNumber, 1);
        } elseif (preg_match('/^8\d{10}$/', $cleanNumber)) {
            return '7' . substr($cleanNumber, 1);
        }else{
            throw \Illuminate\Validation\ValidationException::withMessages([
                'message' => ['field :number is incorrect' . " " . $number]
            ]);
        }
    }

    public function checkDataFromWeb()
    {
        return view('form');
    }
}
