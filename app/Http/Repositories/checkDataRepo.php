<?php

namespace App\Http\Repositories;

use App\Models\Data;
use Illuminate\Database\Eloquent\Model;

class checkDataRepo
{
    public function __construct(Data $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }
}
